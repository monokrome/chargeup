#!/usr/bin/env python
# encoding: utf-8

import os

top = '.'
out = 'build'

message_prefix = '--> '

application_name = 'ChargeUp'
application_version = {
    'major': 0,
    'minor': 0,
    'revision': 0,
    'type': 'prototype'
}

dist_ignored = [
    '**/.waf-*',
    '**/*~',
    '**/*.pyc',
    '**/*.o',
    '**/*.swp',
    '**/*.lock-w',
]

sources_dir = 'src'
sources = [
    'main.cxx',
]

def get_source_path(filename):
    return os.sep.join([sources_dir, filename])

def message(*args):
    print('{0} {1}'.format(message_prefix, ' '.join(args)))

def configure(context):
    message('Configuring {0}'.format(application_name))

    message('Prefix is {0}'.format(context.options.prefix))

def build(context):
        message('Building {0}'.format(application_name))

        sources_with_directories = map(get_source_path, sources)

        context(rule='g++ ${SRC} -o ${TGT}',
                source=sources_with_directories,
                target=application_name.lower())

def dist(context):
    context.excl = ' '.join(dist_ignored)
    context.base_name = '{0}-{1}.{2}.{3}-{4}'.format(
        application_name,
        application_version['major'],
        application_version['minor'],
        application_version['revision'],
        application_version['type']
    )
